﻿.. include:: Includes.txt

.. _start:

=============================================================
Set default author/email
=============================================================

:Classification:
    setdefaultauthor
:Version:
    |release|
:Language:
    en
:Description:
    Defaults the author/email fields in pages and news to the info from the currently logged in user.
:Keywords:
     default, author, news, TYPO3
:Copyright:
    2020
:Author:
    Mike Tölle (T3graf media-agentur)
:Email:
    development@t3graf-media.de
:License:
   This extension documentation is published under the `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons) license

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org
<https://typo3.org/>`_ .


**Table of Contents:**

.. toctree::
   :maxdepth: 3
   :titlesonly:
   :glob:

   Introduction/Index
   Installation/Index
