.. include:: ../Includes.txt


.. _introduction:

============
Introduction
============


.. _what-it-does:

What does it do?
================

This backend extension offers the automatic setting of the logged in user in the author and email field in the page module and in the news extension.


.. _screenshots:

Screenshots
===========

.. figure:: ../Images/EditPageThumb.png
   :width: 600px
   :alt: A screenshot of a Backend form for editing a page

   Editing metadata from page modul
