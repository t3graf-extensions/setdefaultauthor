# TYPO3 Extension `setdefaultauthor`
[![TYPO3 compatibility](https://img.shields.io/static/v1?label=TYPO3&message=v10.4%20%7C%20v11.5&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Latest Stable Version](https://img.shields.io/packagist/v/t3graf/setdefaultauthor?label=stable&color=33a2d8)](https://packagist.org/packages/t3graf/setdefaultauthor)
[![Latest Unstable Version](https://img.shields.io/packagist/v/t3graf/setdefaultauthor?include_prereleases&label=unstable)](https://packagist.org/packages/t3graf/setdefaultauthor)
[![Total Downloads](https://img.shields.io/packagist/dt/t3graf/setdefaultauthor?color=1081c1)](https://packagist.org/packages/t3graf/setdefaultauthor)
[![License](https://img.shields.io/packagist/l/t3graf/setdefaultauthor?color=498e7f)](https://gitlab.com/t3graf-extensions/setdefaultauthor/-/blob/master/LICENSE.md)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/t3graf-extensions/setdefaultauthor/master?label=CI%2FCD&logo=gitlab)](https://gitlab.com/t3graf-extensions/setdefaultauthor/pipelines)

> Defaults the author/email fields in pages to the info from the currently logged in user.

- **Gitlab Repository**: [https://gitlab.com/t3graf-extensions/setdefaultauthor](https://gitlab.com/t3graf-extensions/setdefaultauthor)
- **TYPO3 Extension Repository**: [https://extensions.typo3.org/extension/setdefaultauthor](https://extensions.typo3.org/extension/setdefaultauthor)
- **Found an issue?**: [https://gitlab.com/t3graf-extensions/setdefaultauthor/issues](https://gitlab.com/t3graf-extensions/setdefaultauthor/issues)
## 1. Introduction
### Features

* Based on extbase, implementing best practices from TYPO3 CMS
* Simple and fast installation
* No configuration needed

### Screenshots
#### Page properties metadata
![Page edit](https://cdn.typo3graf-media.de/typo3/setdefaultauthor/edit_page_tmb.png)

- [Fullsize screenshot](https://cdn.typo3graf-media.de/typo3/setdefaultauthor/edit_page_full.png)

## 2. Usage

### Installation
#### Installation using composer
The recommended way to install the extension is by using [Composer][2]. In your Composer based TYPO3 project root, just do `composer require t3graf/setdefaultauthor`.
#### Installation as extension from TYPO3 Extension Repository (TER)
Download and install the extension `setdefaultauthor` with the extension manager module.

## 6. Contribution
Please create an issue at [https://gitlab.com/t3graf-extensions/setdefaultauthor/issues](https://gitlab.com/t3graf-extensions/setdefaultauthor/issues).

**Please use Gittlab only for bug-reporting or feature-requests. For support use the TYPO3 community channels or contact us by email.**

## 7. Support

If you need privat or personal support, contact us by email on [support@t3graf-media.de](support@t3graf-media.de)

**Be aware that this support might not be free!**

[1]: https://docs.typo3.org/typo3cms/extensions/setdefaultauthor/
[2]: https://getcomposer.org
