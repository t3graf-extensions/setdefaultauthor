# 11.0.1

## BUGFIX

- [BUGFIX] fix replace problem in composer.json f5def5a
- [BUGFIX] fix problems with composer.json 62da5ca

# 11.0.0

## TASK

- [TASK] Add official support for TYPO3 11.5 LTS bbf431e
- [TASK] change gitlab repository 8ca0f43

## MISC

- Initial commit fc854a1

