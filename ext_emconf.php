<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
  'title' => 'Set default author/email',
  'description' => 'Defaults the author/email fields in pages and sys_notes to the info from the currently logged in user',
  'category' => 'be',
  'version' => '11.0.1',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearcacheonload' => true,
  'author' => 'Mike Tölle',
  'author_email' => 'kontakt@t3graf-media.de',
  'author_company' => 'T3graf media-agentur UG',
  'constraints' =>
  [
    'depends' =>
    [
      'typo3' => '10.4.0-11.5.99',
    ],
    'conflicts' =>
    [
    ],
    'suggests' =>
    [
    ],
  ],
];
