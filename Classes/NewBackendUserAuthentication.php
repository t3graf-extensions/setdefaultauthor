<?php

/*
 * This file is part of the package t3graf/extended_bootstrap-package.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\Setdefaultauthor;

/***************************************************************
 *  Copyright notice
 *  (c) 2015 Mike Tölle <mike.toelle@t3graf.de>, T3graf media-agentur UG
 *  All rights reserved
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Configuration\TypoScript\ConditionMatching\ConditionMatcher;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class NewBackendUserAuthentication extends BackendUserAuthentication
{
    protected function prepareUserTsConfig(): void
    {
        $collectedUserTSconfig = [
            'default' => $GLOBALS['TYPO3_CONF_VARS']['BE']['defaultUserTSconfig']
        ];

        // Default TSconfig for admin-users
        if ($this->isAdmin()) {
            $collectedUserTSconfig[] = 'admPanel.enable.all = 1';
        }
        // Setting defaults for author / email
        if (ExtensionManagementUtility::isLoaded('news')) {
            $collectedUserTSconfig[] = '
TCAdefaults.tx_news_domain_model_news.author = ' . $this->user['realName'] . '
TCAdefaults.tx_news_domain_model_news.author_email = ' . $this->user['email'];
        }
        $collectedUserTSconfig[] = '
TCAdefaults.sys_note.author = ' . $this->user['realName'] . '
TCAdefaults.sys_note.email = ' . $this->user['email'];
        $collectedUserTSconfig[] = '
TCAdefaults.pages.author = ' . $this->user['realName'] . '
TCAdefaults.pages.author_email = ' . $this->user['email'];
        // Loop through all groups and add their 'TSconfig' fields
        $typo3Version = GeneralUtility::makeInstance(Typo3Version::class);
        if ($typo3Version->getMajorVersion() === 10) {
            foreach ($this->includeGroupArray as $groupId) {
                $collectedUserTSconfig['group_' . $groupId] = $this->userGroups[$groupId]['TSconfig'] ?? '';
            }
        } else {
            foreach ($this->userGroupsUID as $groupId) {
                $collectedUserTSconfig['group_' . $groupId] = $this->userGroups[$groupId]['TSconfig'] ?? '';
            }
        }
        $collectedUserTSconfig[] = $this->user['TSconfig'];

        // Check external files
        $collectedUserTSconfig = TypoScriptParser::checkIncludeLines_array($collectedUserTSconfig);
        // Imploding with "[global]" will make sure that non-ended confinements with braces are ignored.
        $userTS_text = implode("\n[GLOBAL]\n", $collectedUserTSconfig);
        // Parsing the user TSconfig (or getting from cache)
        $hash = md5('userTS:' . $userTS_text);
        $cache = GeneralUtility::makeInstance(CacheManager::class)->getCache('hash');
        if (!($this->userTS = $cache->get($hash))) {
            $parseObj = GeneralUtility::makeInstance(TypoScriptParser::class);
            $conditionMatcher = GeneralUtility::makeInstance(ConditionMatcher::class);
            $parseObj->parse($userTS_text, $conditionMatcher);
            $this->userTS = $parseObj->setup;

            $cache->set($hash, $this->userTS, ['UserTSconfig'], 0);
            // Ensure to update UC later
            $this->userTSUpdated = true;
        }
    }
}
